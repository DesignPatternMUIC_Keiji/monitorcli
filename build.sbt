val ScalatraVersion = "2.6.2"

organization := "muic.design.pattern"

name := "MonitorCLI"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.12.4"

resolvers ++= Seq(
  Classpaths.typesafeReleases,
  "Hyperreal Repository" at "https://dl.bintray.com/edadma/maven"
)

libraryDependencies ++= Seq(
  "org.scalatra" %% "scalatra" % ScalatraVersion,
  "org.scalatra" %% "scalatra-scalatest" % ScalatraVersion % "test",
  "ch.qos.logback" % "logback-classic" % "1.1.5" % "runtime",
  "org.eclipse.jetty" % "jetty-webapp" % "9.2.15.v20160210" % "container",
  "javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
  "xyz.hyperreal" %% "b-tree" % "0.3",
  "org.json4s"   %% "json4s-jackson" % "3.5.2",
  "com.roundeights" %% "hasher" % "1.2.0",
  "org.scalatra" %% "scalatra-json" % ScalatraVersion,
  "org.apache.httpcomponents" % "httpclient" % "4.5.3",
)
// https://mvnrepository.com/artifact/org.pcap4j/pcap4j
libraryDependencies ++= Seq(
  "net.debasishg" %% "redisclient" % "3.4"
)

enablePlugins(JettyPlugin)

containerPort in Jetty := 8081

enablePlugins(SbtTwirl)
enablePlugins(ScalatraPlugin)
