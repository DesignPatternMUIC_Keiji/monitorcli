package muic.design.pattern.model


case class Information(cpuUsage: Double, requestPerSecond: Double)
