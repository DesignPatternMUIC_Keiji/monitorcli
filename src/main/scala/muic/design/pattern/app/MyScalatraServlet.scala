package muic.design.pattern.app

import org.scalatra._
import org.json4s.{DefaultFormats, Formats}
import org.scalatra.json._
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._
import java.net._

import muic.design.pattern.Service.JsonService
import muic.design.pattern.model.KeyIP
import muic.design.pattern.thread.TimerThread
import org.scalatra._

import org.scalatra.CorsSupport

class MyScalatraServlet extends ScalatraServlet with JacksonJsonSupport with CorsSupport  {
  implicit val jsonFormats: Formats = DefaultFormats
  options("/*"){
    response.setHeader(
      "Access-Control-Allow-Headers", request.getHeader("Access-Control-Request-Headers"));
  }
  before() {
    contentType = formats("json")
  }


  post("/connect") {
    val jsonString = request.body
    println(jsonString)
    if (!request.body.isEmpty) {
      val jValue = parse(jsonString)
      val kip = jValue.extract[KeyIP]
      println(kip.key)
      //maybe it is better to not synchronized this whole thing but i couldnt be half arsed
      this.synchronized{
        //start the servlet for the first connect
        if(MyScalatraServlet.keyIPList.isEmpty){
          val tr1 = new TimerThread()
          tr1.start()
        }
        if (!MyScalatraServlet.keyIPList.map(_.ip).contains(kip.ip)){
          MyScalatraServlet.keyIPList += KeyIP(kip.ip,kip.port,kip.key)
          MyScalatraServlet.jsonService.makeSuccess()
        }else{
          MyScalatraServlet.jsonService.makeFailure("no body provided")
        }
      }

    }else{
      MyScalatraServlet.jsonService.makeFailure("no body provided")
    }
  }


  notFound {
    NotFound("Sorry, the application does not support the API you are giving")
  }

}

object MyScalatraServlet {
  //Vector[KeyIP]
  val keyIPList: collection.mutable.ListBuffer[KeyIP] = collection.mutable.ListBuffer[KeyIP]()
  val jsonService = new JsonService
}
