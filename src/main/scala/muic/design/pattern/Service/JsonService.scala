package muic.design.pattern.Service

import org.json4s.jackson.JsonMethods.{compact, render}
import org.json4s.JsonDSL._
import muic.design.pattern.model.Information
import org.json4s.{DefaultFormats, Formats}

class JsonService {
  implicit val jsonFormats: Formats = DefaultFormats

  def makeSuccess(): String = compact(render("result" -> "success"))
  def makeFailure(msg: String): String = compact(render("result" -> "failure: " + msg))
  def makeInformationJson(info: Information, key: String): String = {
    val jsonString = ("key" -> key) ~ ("cpuUsage" -> info.cpuUsage) ~ ("requestPerSec" -> info.requestPerSecond)
    compact(render(jsonString))
  }
}
