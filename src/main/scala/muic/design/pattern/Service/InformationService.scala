package muic.design.pattern.Service


import java.lang.management.ManagementFactory

import muic.design.pattern.model.Information
import com.redis._

class InformationService {
  val client = new RedisClient("localhost", 6379)

  def getInformation(): Information = {
//    val physicalMemUsage = bean.getFreePhysicalMemorySize / bean.getTotalPhysicalMemorySize * 100
    val serverInfo = client.info.get
    val cpuLoad = getValueFromInfoString(serverInfo,"used_cpu_sys").toDouble + getValueFromInfoString(serverInfo,"used_cpu_user").toDouble
    val requestPerSec = getValueFromInfoString(serverInfo,"instantaneous_ops_per_sec").toDouble

    new Information(cpuLoad,requestPerSec)
  }
  def getValueFromInfoString(serverInfo: String, param: String): String = {
    serverInfo.split("\r\n").filter(a => a.startsWith(param)).head.split(":").tail.head
  }
}

//object app extends App {
//  val info = new InformationService
//  val serverInfo = info.client.info
//  val requestPerSec = serverInfo.get.split("\n").filter(a => a.startsWith("instantaneous_ops_per_sec")).head.split(":").tail.head
//  println(requestPerSec)
////  Thread.sleep(10000)
//////  println(info.getInformation())
//////  Thread.sleep(10000)
//////  println(info.getInformation())
//////  Thread.sleep(10000)
//////  println(info.getInformation())
//////  Thread.sleep(10000)
//////  println(info.getInformation())
//////  Thread.sleep(10000)
//////  println(info.getInformation())
//
//
//}
