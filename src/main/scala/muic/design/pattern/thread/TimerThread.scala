package muic.design.pattern.thread

import java.net.InetAddress

import muic.design.pattern.Service._
import muic.design.pattern.app.MyScalatraServlet
import muic.design.pattern.model.{Information, KeyIP}
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClientBuilder

import scala.concurrent.Promise


class TimerThread() extends Thread{
  /**
    * This thread will be started once the first connection is received
    * it will
    * Services Used: CommunicationService, InformationService, JsonService
    * 1) at each time interval it will synchronized take the ips of the places to send the information to
    * 2) call InformationService to get the the instance of the InformationModel to return
    * 3) parse the InformationModel into Json through JsonService
    * 4) Send the json value into the communication service with the ip to send the information to that IP.
    * 5) Send the thread into sleep and wait for the next instruction loop
    */
  val informationService  = new InformationService
  val jsonService = new JsonService
  override def run(): Unit ={
    while (true) {
      var kl: collection.mutable.ListBuffer[KeyIP] = collection.mutable.ListBuffer[KeyIP]()
      //1
      this.synchronized{
        kl = MyScalatraServlet.keyIPList
      }
      //sleep if kl is empty, there's no point doing anything
      if (kl.isEmpty) {
        Thread.sleep(10000)
      }else{
        //2
        val information = informationService.getInformation()

        //3
        println("sending info")
        kl.foreach(kip => {

          val ip = kip.ip
          val port = kip.port
          val key = kip.key
          println(kip.ip + " " + kip.port + kip.key)
          val post = new HttpPost(s"http://$ip:$port/client/response")
          println(information.cpuUsage + " " + information.requestPerSecond)
          val jsonString = jsonService.makeInformationJson(information,kip.key)
          post.setHeader("Content-type", "application/json")
          post.setHeader("key",key)
          post.setEntity(new StringEntity(jsonString))
          val client = HttpClientBuilder.create().build()
          val ans = client.execute(post)
          println(ans.getStatusLine.getStatusCode)
          client.close()
        })
        println("sent info")

        //4
        Thread.sleep(2000)
      }
    }
  }

}

//object TimerThread extends App {
//  val post = new HttpPost("localhost:8080")
//  post.setHeader("Content-type", "application/json")
//  post.setHeader("key","hi")
//  println(post.getAllHeaders.tail.head.getValue)
//}